import mongoose from 'mongoose';

const db = mongoose.connection;
mongoose.connect('mongodb://localhost:27017/test');

db.on('connected', () => {
  console.log('Mongodb connected success');
});

db.on('error', () => {
  console.log('Mongdb connected failed');
});

process.on('SIGINT', async () => {
  await db.close();
  process.exit(0);
});
export default db;
