import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

function newConnection(uri) {
  const db = mongoose.connection;

  mongoose.connect(uri);
  db.on('connected', () => {
    console.log('Mongodb connected success');
  });

  db.on('error', () => {
    console.log('Mongdb connected failed');
  });

  process.on('SIGINT', async () => {
    await db.close();
    process.exit(0);
  });

  return db;
}

// make connection to multiple db
// const testConnection = newConnection('mongodb://localhost:27017/test');
const userConnection = newConnection(process.env.URI_MONGODB_USER);

const dbConnections = {
  // testConnection,
  userConnection,
};

export default dbConnections;
