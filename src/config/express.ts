import express, { Response, Request, NextFunction } from 'express';
import * as dotenv from 'dotenv';
import { type ValidationError as JoiValidationError } from 'joi';
import { ValidationError } from 'express-validation';
import httpStatus from 'http-status';
import cookieParse from 'cookie-parser';
import cors from 'cors';
import router from '../server/index.route';
import errorcode from '../constants/errorCode';
import APIError from '../helpers/APIError';

dotenv.config();

const app = express();
app.use(express.json());
app.use(cookieParse()); // đọc cookie từ request
app.use(
  cors({
    origin: [],
    credentials: true, // yêu cầu client gửi kèm cookie
  }),
);
app.use(express.urlencoded({ extended: true }));

app.use('/api', router);

app.use((err: Error | ValidationError, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof ValidationError) {
    const detailsBody: JoiValidationError[] | undefined = err.details.body;
    const detailsQuery: JoiValidationError[] | undefined = err.details.query;
    if (detailsBody) {
      const errorMessageArr: string[] = detailsBody.map((thizBody) => thizBody.message);
      const error = new APIError(errorMessageArr.join(', '), httpStatus.BAD_REQUEST, errorcode.VALIDATE);
      next(error);
      return;
    }
    if (detailsQuery) {
      const errorMessageArr: string[] = detailsQuery.map((thizBody) => thizBody.message);
      const error = new APIError(errorMessageArr.join(', '), httpStatus.BAD_REQUEST, errorcode.VALIDATE);
      next(error);
      return;
    }
    const error = new APIError(err.message, httpStatus.BAD_REQUEST, errorcode.VALIDATE);
    next(error);
    return;
  }
  if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, errorcode.EXCEPTION, '', err.stack);
    next(apiError);
    return;
  }
  // the rest is 404
  next(err);
});

export default app;
