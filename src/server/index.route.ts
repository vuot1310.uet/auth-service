import { Router } from 'express';
import httpStatus from 'http-status';
import jsonwebtoken from 'jsonwebtoken';
import UserRouter from './api/user/user.route';

const router = Router();

router.route('/').get((req, res) => {
  res.send('Hello World!');
});

router.route('/health-check').get((req, res) => {
  res.send('Healthy!');
});

// eslint-disable-next-line consistent-return
router.route('/auth/introspect').post((req, res) => {
  const { token } = req.body;
  const tokenBearer = token.split(' ')[1];
  if (!token) {
    return res.status(httpStatus.OK).json({
      active: false,
    });
  }
  jsonwebtoken.verify(tokenBearer, process.env.ACCESS_TOKEN_SECRET, (err) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        return res.status(httpStatus.OK).json({
          active: false,
        });
      }
      return res.status(httpStatus.OK).json({
        active: false,
      });
    }
    return res.status(httpStatus.OK).json({
      active: true,
    });
  });
});
router.use('/auth', UserRouter);

export default router;
