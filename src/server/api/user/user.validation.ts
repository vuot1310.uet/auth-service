import { Joi } from 'express-validation';

const registerValidation = {
  body: Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  }),
};

const loginValidation = {
  body: Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  }),
};

const UserValidator = {
  registerValidation,
  loginValidation,
};

export default UserValidator;
