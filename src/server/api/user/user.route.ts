import express from 'express';
import { validate } from 'express-validation';
import UserController from './user.controller';
import UserValidator from './user.validation';
import authToken from '../../../helpers/authToken';

const UserRouter = express.Router();

UserRouter.route('/register').post(validate(UserValidator.registerValidation), UserController.register);
UserRouter.route('/login').post(validate(UserValidator.loginValidation), UserController.login);
UserRouter.route('/listEmails').get(authToken.verifyAccessToken, UserController.getListUsers);
UserRouter.route('/refresh-token').get(UserController.refreshToken);
UserRouter.route('/logout').delete(UserController.logout);

export default UserRouter;
