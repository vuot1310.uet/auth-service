import dotenv from 'dotenv';
// import './config/connectDB';
import './config/connectMultipleDB';
import './helpers/redisClient';
import app from './config/express';

dotenv.config();

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`Express is listening at http://localhost:${port}`);
});
