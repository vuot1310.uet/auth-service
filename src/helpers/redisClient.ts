/* eslint-disable no-restricted-syntax */

import Redis from 'ioredis';

const client = new Redis({
  port: Number(process.env.REDIS_PORT),
  host: process.env.REDIS_HOST,
  // password: process.env.REDIS_PASS,
});

client.on('error', (err) => {
  console.error(err);
});

client.on('connect', () => {
  console.log('Redis connected');
});

client.on('ready', () => {
  console.log('Redis is ready');
});

const get = async (key: string): Promise<any> => {
  const reply = await client.get(key);
  return reply;
};

const set = async (key: string, value: any, ttl: number): Promise<any> => {
  const reply = await client.set(key, value);
  await client.expire(key, ttl);
  return reply;
};

const del = async (key: string): Promise<any> => {
  const reply = await client.del(key);
  return reply;
};

export default {
  get,
  set,
  del,
};
