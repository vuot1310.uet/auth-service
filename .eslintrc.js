module.exports = {
    env: {
      node: true,
      jest: true,
    },
    extends: [
      'airbnb-base',
      'airbnb-typescript/base',
      'plugin:@typescript-eslint/recommended',
      'plugin:prettier/recommended',
    ],
    ignorePatterns: ['.eslintrc.js'],
    parser: '@typescript-eslint/parser',
    parserOptions: {
      project: 'tsconfig.json',
      sourceType: 'module',
    },
    plugins: ['@typescript-eslint/eslint-plugin'],
    root: true,
    rules: {
      'import/extensions': 0,
      'max-len': [
        'error',
        120,
      ],
      '@typescript-eslint/no-non-null-assertion': 0,
      'prettier/prettier': [
        'error',
        {
          'endOfLine': 'auto',
        }
      ],
    },
  };
  